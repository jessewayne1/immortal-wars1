﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace myfirstapp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Start_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You have started the game. The game will now load", "Start");
            this.timer1.Start();
            //execute the background worker DoWork()
            backgroundWorker1.RunWorkerAsync();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Are you sure?", "Quit");

            // display
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            this.timer1.Start();
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.progressBar1.Increment(5);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //there should be no GUI component method
            for (int i = 0; i <= 100; i++)
            {
                simulateheavyWork();
                backgroundWorker1.ReportProgress(i);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //this is updated from dowork.Its where GUI components are updated
            //recieves updates after 100 ms
            progressBar1.Value = e.ProgressPercentage;
            Percentage.Text = e.ProgressPercentage.ToString() + " %";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //called when the heavy operation in bg is over.Can also accept GUI components
            display("Loading Complete.");

        } 

        //simulate complex calculations
        private void simulateheavyWork()
        {
            System.Threading.Thread.Sleep(100);
        }
    }
}
